pytest and continuous integration with gitlab demo
by Jon Pierre


1)	Fork https://gitlab.com/jpierre89/sunset-theater

2)	Clone, setup, and verify it runs on your pc.

note:
pytest passes with no tests ran. Notice how there are no warnings.


4)	Create ‘conftest.py’ at project root (So pytest adds the root directory to its search path)
	Create a directory for testing at project root called tests (can be named anything)
	Create conftest.py in the tests directory (Contents include testing setup code)
	Create ‘test_demo.py’ in the tests directory

note:
all test files must be in the form ‘test_*’ where * can be anyting. This is how pytest locates test files.


5)	Copy the following into conftest.py (in tests directory)

import pytest
from app import app

@pytest.fixture(scope="session")
def client():
    app.config.update(TESTING=True)

    with app.test_client() as client:
        yield client
	
note:
after runnig pytest again, notice how pytest gives you a warning summary from instantiating the app, even though there are no tests for it to run.

6) 	Copy the following code into test_demo.py


def test_post_seating_no_auth(client):
    assert client.get('/seat/reserved', data={'show_id': '1', 'seat_id': '1'}).status_code == 401
	run pytest and see your first passing test. The status code returned was 401 which was expected because you did not provide authorization (if you look at my route it has a decorator requiring a jwt token).





7) 	copy the following into test_demo.py
	
from flask_jwt_extended.utils import create_access_token
from app import app


def get_header():
    with app.app_context():
        access_toke = create_access_token('testuser')
    head = {
        'Authorization': 'Bearer {}'.format(access_toke)
    }
    return head

def test_get_seating_authorized(client):
    assert client.get('/seat/reserved',
                      headers=get_header(),
                      data={'show_id': '1', 'seat_id': '1'}).status_code == 200

	See how you can create jwt token to send in header and be authorized to access routes that are protected. This get_header() method could go in conftest.py to be shared by all testing files.



10)	Create a ci config file for use with gitlab.
	This file is recognized by gitlab when you push to them.
	Name it .gitlab-ci.yml
	copy the following into that file:

image: python:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python -V
  - pip3 install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip3 install -r requirements.txt
  - pip3 list
  - ls -la
  - chmod +x main.py
  - echo main.py
  
test:
  script:
    - pytest

	add/commit/push to gitlab and watch the pipeline automatically run. On gitlab, to see pipeline, either go to go to repo and click blue circle (running pipeline) or go to ci/cd in settings.

Note:
- gitlab cache decreases the time it takes for your pipelines to run by caching previous dependency installations.
- This is (maybe) important because our group is limited to 2000 ci minutes per month.
- Initially, cache will fail because it does not yet exist. Also, Your test must succeed in order for packages to be cached by gitlab. You will see that iw uploads the cach.zip with dependencies when your push succeeds.

11)	Add a comment somewhere, add/commit/push and notice again look at the pipeline. Notice how the dependencies are already satisfied, and the pipeline is much faster.

12) In Gitlab repo, go to Settings -> General -> merge requests, and notice the box 'pipelines must succeeed'. 
    A Maintainer should check this box for every repo in our group, so if the pipeline fails, the push will not be successful, and the pusher will need to fix all failed tests.
